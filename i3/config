set $mod Mod4
set $up Up
set $down Down
set $left Left
set $right Right

bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1; workspace 1
bindsym $mod+Shift+2 move container to workspace 2; workspace 2
bindsym $mod+Shift+3 move container to workspace 3; workspace 3
bindsym $mod+Shift+4 move container to workspace 4; workspace 4
bindsym $mod+Shift+5 move container to workspace 5; workspace 5
bindsym $mod+Shift+6 move container to workspace 6; workspace 6
bindsym $mod+Shift+7 move container to workspace 7; workspace 7
bindsym $mod+Shift+8 move container to workspace 8; workspace 8
bindsym $mod+Shift+9 move container to workspace 9; workspace 9
bindsym $mod+Shift+0 move container to workspace 10; workspace 10


#####################################################################################################################
#################                          how to exit, logoff, suspend, ...                        #################
#####################################################################################################################
bindsym $mod+Shift+e exec --no-startup-id "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+X mode "$mode_system"
bindsym control+mod1+Delete mode "$mode_system"

set $mode_system System (k) lock, (l) logout, (u) suspend, (h) hibernate, (r) reboot, (s) shutdown
mode "$mode_system" {
    bindsym k exec --no-startup-id ~/.config/i3/scripts/i3exit.sh lock, mode "default"
    bindsym l exec --no-startup-id ~/.config/i3/scripts/i3exit.sh logout, mode "default"
    bindsym u exec --no-startup-id ~/.config/i3/scripts/i3exit.sh suspend, mode "default"
    bindsym h exec --no-startup-id ~/.config/i3/scripts/i3exit.sh hibernate, mode "default"
    bindsym r exec --no-startup-id ~/.config/i3/scripts/i3exit.sh reboot, mode "default"
    bindsym s exec --no-startup-id ~/.config/i3/scripts/i3exit.sh shutdown, mode "default"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

#####################################################################################################################
#################                          reload changed configuration                             #################
#####################################################################################################################

bindsym $mod+Shift+r restart
bindsym $mod+Shift+c reload

#####################################################################################################################
#################                          Stopping an application                                  #################
#####################################################################################################################

bindsym $mod+Shift+q kill
bindsym $mod+q kill

#####################################################################################################################
#################                          Moving around in i3                                      #################
#####################################################################################################################

floating_modifier $mod
bindsym $mod+Shift+space floating toggle
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

#####################################################################################################################
#################                          moving around workspaces                                 #################
#####################################################################################################################

bindsym Mod1+Tab workspace next
bindsym Mod1+Shift+Tab workspace prev
bindsym $mod+Tab workspace back_and_forth
bindsym Mod1+Ctrl+Right workspace next
bindsym Mod1+Ctrl+Left workspace prev
for_window [urgent=latest] focus

#####################################################################################################################
#################                          Tiling parameters                                        #################
#####################################################################################################################

default_orientation horizontal
bindsym $mod+h split h
bindsym $mod+v split v
bindsym $mod+f fullscreen toggle
bindsym $mod+s layout stacking
bindsym $mod+z layout tabbed
bindsym $mod+e layout toggle split
bindsym $mod+space focus mode_toggle
bindsym $mod+a focus parent

#####################################################################################################################
#################                          resize                                                   #################
#####################################################################################################################

bindsym $mod+r mode "resize"
mode "resize" {
        bindsym $left       resize shrink width 10 px or 10 ppt
        bindsym $down       resize grow height 10 px or 10 ppt
        bindsym $up         resize shrink height 10 px or 10 ppt
        bindsym $right      resize grow width 10 px or 10 ppt
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

#####################################################################################################################
#################                          choose the font                                          #################
#####################################################################################################################

#font pango:Noto Mono Regular 13
font pango:monospace 8

#####################################################################################################################
#################                          assign applications to workspaces                       #################
#####################################################################################################################

assign [class="Firefox|firefox"]                                                    → 2
assign [class="Chromium|Google-chrome"]                                             → 2
for_window [class="firefox|Firefox"] move to workspace 2
assign [class="sublime-text|sublime_text|Sublime_text|subl|Subl|subl3|Subl3|Code|code"]       → 3
assign [class="Thunar"]                                 → 4

#####################################################################################################################
#################                          execute applications at boot time                        #################
#####################################################################################################################

exec --no-startup-id firefox
for_window [class="firefox"] focus
exec --no-startup-id nm-applet
exec --no-startup-id clipit

#####################################################################################################################
#################                          applications keyboard shortcuts                          #################

#conky
#bindsym $mod+c exec --no-startup-id conky-toggle
#bindsym control+mod1+Next exec --no-startup-id conky-rotate -n
#bindsym control+mod1+Prior exec --no-startup-id conky-rotate -p
#xkill
bindsym --release $mod+Escape exec xkill
bindsym $mod+Return exec lxterminal
bindsym $mod+w exec firefox
bindsym $mod+Shift+f exec thunar
bindsym $mod+Shift+s exec subl
bindsym $mod+Shift+d exec --no-startup-id jgmenu_run
bindsym $mod+d exec --no-startup-id i3-dmenu-desktop 
#--dmenu=-nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'

#####################################################################################################################
#################                          screenshots                                              #################
#####################################################################################################################

bindsym Print exec --no-startup-id scrot 'hefa-%Y-%m-%d-%s_screenshot_$wx$h.jpg' -e 'mv $f $$(xdg-user-dir PICTURES)'
bindsym Control+Print exec --no-startup-id xfce4-screenshooter

#####################################################################################################################
#################                          floating or tiled                                        #################
#####################################################################################################################

for_window [instance="vlc"] floating enable
for_window [instance="qalculate"] floating enable
for_window [class="jgmenu"] floating enable
for_window [title="Copying"] floating enable
for_window [title="Deleting"] floating enable
for_window [title="Moving"] floating enable
for_window [title="^Terminator Preferences$"] floating enable
for_window [window_role="pop-up"] floating enable
for_window [window_role="^Preferences$"] floating enable
for_window [window_role="setup"] floating enable

#####################################################################################################################
#################                          bar toggle                                               #################
#####################################################################################################################

bindsym $mod+b bar mode toggle

#####################################################################################################################
#################                          border control                                           #################
#####################################################################################################################

hide_edge_borders both
bindsym $mod+shift+b exec --no-startup-id i3-msg border toggle
bindsym $mod+t border normal
bindsym $mod+y border 1pixel
bindsym $mod+u border none
new_window normal
new_float normal

#####################################################################################################################
#################                          compton of i3wm                                          #################
#####################################################################################################################

#if you want transparency on non-focused windows, ...
exec_always --no-startup-id picom --config ~/.config/i3/picom.conf

#####################################################################################################################
#################                          bar appearance                                           #################
#####################################################################################################################

bar {
	position bottom
    status_command i3blocks -c ~/.config/i3/i3blocks.conf
    colors {
        separator #268bd2
        background #002b36
        statusline #839496
        focused_workspace #fdf6e3 #6c71c4 #fdf6e3
        active_workspace #fdf6e3 #6c71c4 #fdf6e3
        inactive_workspace #002b36 #586e75 #002b36
        urgent_workspace #d33682 #d33682 #fdf6e3
    }
}
#client.placeholder      #242424 #242424 #242424
#client.background       #242424 #242424 #242424
#client.focused          #6790EB #6790EB #e5e5e5 #6790EB
#client.unfocused        #222222 #222222 #aaaaaa #222222
#client.focused_inactive #222222 #222222 #a9a9a9 #222222
client.urgent           #d42121 #d42121 #f7f7f7 #d42121

#####################################################################################################################
#################                          Scratchpad                                               #################
#####################################################################################################################

# NOT USED

# move the currently focused window to the scratchpad
# bindsym Mod1+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
# bindsym Mod1+minus scratchpad show


#####################################################################################################################
#################                          keyboard control                                         #################
#####################################################################################################################
exec --no-startup-id setxkbmap -layout us,ir -option "grp:alt_shift_toggle"
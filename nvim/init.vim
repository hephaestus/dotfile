if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd!
  autocmd VimEnter * PlugInstall
endif

cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
let maplocalleader= ';'
set encoding=utf-8
set nobackup
set nowritebackup
set cmdheight=1
set updatetime=300
" set hidden
set shortmess+=c
set signcolumn=number
set laststatus=2
set showtabline=2 
set guioptions-=e

call plug#begin('~/.config/nvim/plugged')
"File Search:
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
"File Browser:
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'mkitt/tabline.vim'
Plug 'ryanoasis/vim-devicons'
"Color:
Plug 'morhetz/gruvbox'
"Golang:
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
"Autocomplete:
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Snippets:
"Git:
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
"StatusBar:
Plug 'itchyny/lightline.vim'
Plug 'mgee/lightline-bufferline' " For tabs on top

call plug#end()

"COPY/PASTE:
"-----------
"Increases the memory limit from 50 lines to 1000 lines
:set viminfo='100,<1000,s10,h

"NUMBERING:
"----------
set number

"INDENTATION:
"------------
"Highlights code for multiple indents without reselecting
vnoremap < <gv
vnoremap > >gv

"COLOR:
"------
colorscheme gruvbox


"AUTOCOMPLETE:
"-------------

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
      
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()


" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm() : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> U :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &keywordprg . " " . expand('<cword>')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)


" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>

"LightLine StatusBar:
let g:lightline = {
	\ 'active': {
	\    'left':[ ['bnumber', 'mode', 'paste' ],['gitbranch', 'readonly', 'modified', 'filename', 'currentfunction', 'cocstatus', 'cowar']],
	\    'right': [ [ 'lineinfo' ], [ 'percent' ],[ 'fileformat', 'fileencoding', 'filetype'] ]
	\ },
  \ 'tabline': {
  \   'left': [['buffers']],
  \   'right': [['close']],
  \ },
	\ 'component': {
  \    'lineinfo': '⭡ %3l:%-2v',
  \ },
  \ 'component_expand': {
  \   'buffers': 'lightline#bufferline#buffers'
  \ },
	\ 'component_function': {
  \   'bnumber': 'LightlineMode',
	\   'gitbranch':'LightlineGit',
	\   'readonly': 'LightLineReadonly',
	\   'modified': 'LightLineModified',
	\   'filename': 'LightLineFname',
	\   'filetype': 'LightLineFiletype',
	\   'fileformat': 'LightLineFileformat',
  \   'cocstatus': 'LightLineCocError',
  \   'cocwar':    'LightLineCocWarn',
  \   'currentfunction': 'CocCurrentFunction'
	\ },
	\ 'component_type': {'buffers': 'tabsel'},
	\ 'subseparator': { 'left': ' ▎', 'right': ' ▎' }
\}

function! LightlineMode()
  let nr = s:get_buffer_number()
  let nmap = [ '', '❶', '❷', '❸', '❹', '❺', '❻', '❼', '❽', '❾', '➓' ]
  let num = nmap[nr]
  if nr == 0
    return ''
  endif
  return num
endfunction
function! s:get_buffer_number()
  let i = 0
  for nr in filter(range(1, bufnr('$')), 'bufexists(v:val) && buflisted(v:val)')
    let i += 1
    if nr == bufnr('')
      return i
    endif
  endfor
  return ''
endfunction
function! LightlineGit()
    let [a,m,r] = GitGutterGetHunkSummary()
    let gutter = printf('+%d ~%d -%d', a, m, r)
    let head = fugitive#head()
    return empty(head) ? '~' : ' ' . head . ' ' . gutter
endfunction

function! LightLineModified()
  if &filetype == "help"
    return ""
  elseif &modified
    return "+"
  elseif &modifiable
    return ""
  else
    return ""
  endif
endfunction
function! LightLineReadonly()
  if &filetype == "help"
    return ""
  elseif &readonly
    return ""
  else
    return ""
  endif
endfunction
function! LightLineCocError()
  let error_sign = get(g:, 'coc_status_error_sign', has('mac') ? '❌ ' : 'E')
  let info = get(b:, 'coc_diagnostic_info', {})
  if empty(info)
    return ''
  endif
  let errmsgs = []
  if get(info, 'error', 0)
    call add(errmsgs, error_sign . info['error'])
  endif
  return trim(join(errmsgs, ' ') . ' ' . get(g:, 'coc_status', ''))
endfunction
function! LightLineCocWarn() abort
  let warning_sign = get(g:, 'coc_status_warning_sign')
  let info = get(b:, 'coc_diagnostic_info', {})
  if empty(info)
    return ''
  endif
  let warnmsgs = []
  if get(info, 'warning', 0)
    call add(warnmsgs, warning_sign . info['warning'])
  endif
  return trim(join(warnmsgs, ' ') . ' ' . get(g:, 'coc_status', ''))
endfunction
function! CocCurrentFunction()
    return get(b:, 'coc_current_function', '')
endfunction
function! LightLineFname() 
  let icon = (strlen(&filetype) ? ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') 
  let filename = LightLineFilename()
  let ret = [filename,icon]
  if filename == ''
    return ''
  endif
  return join([filename, icon],'')
endfunction
function! LightLineFilename()
  return ('' != LightLineReadonly() ? LightLineReadonly() . ' ' : '') .
        \ ('' != expand('%:t') ? expand('%:t') : '') .
        \ ('' != LightLineModified() ? ' ' . LightLineModified() : '')
endfunction
function! LightLineFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
endfunction
function! LightLineFileformat()
  return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
endfunction

autocmd User CocDiagnosticChange call lightline#update()
let g:lightline.colorscheme = 'molokai'
let g:lightline#bufferline#show_number  = 2
let g:lightline#bufferline#shorten_path = 1
let g:lightline#bufferline#enable_devicons = 1
let g:lightline#bufferline#filename_modifier = ':t'
let g:lightline#bufferline#unnamed      = '[No Name]'
let g:lightline#bufferline#number_map = {
      \ 0: '➓ ', 1: '❶ ', 2: '❷ ', 3: '❸ ', 4: '❹ ',
      \ 5: '❺ ', 6: '❻ ', 7: '❼ ', 8: '❽ ', 9: '❾ '}

nmap <LocalLeader>1 <Plug>lightline#bufferline#go(1)
nmap <LocalLeader>2 <Plug>lightline#bufferline#go(2)
nmap <LocalLeader>3 <Plug>lightline#bufferline#go(3)
nmap <LocalLeader>4 <Plug>lightline#bufferline#go(4)
nmap <LocalLeader>5 <Plug>lightline#bufferline#go(5)
nmap <LocalLeader>6 <Plug>lightline#bufferline#go(6)
nmap <LocalLeader>7 <Plug>lightline#bufferline#go(7)
nmap <LocalLeader>8 <Plug>lightline#bufferline#go(8)
nmap <LocalLeader>9 <Plug>lightline#bufferline#go(9)
nmap <LocalLeader>0 <Plug>lightline#bufferline#go(10)

"VIM-GO
"-------------
let g:go_fmt_command = "goimports"
let g:go_def_mapping_enabled = 0
augroup vgo
  au FileType go nmap gtj :CocCommand go.tags.add json<cr>
  au FileType go nmap gtb :CocCommand go.tags.add bson<cr>
  au FileType go nmap gtx :CocCommand go.tags.clear<cr>
  au FileType go nmap <LocalLeader>imp :call CocAction('organizeImport')
  au FileType go nmap <LocalLeader>ci :CocCommand go.impl.cursor<cr>
	au FileType go nmap <LocalLeader>c :GoCallers<CR>
	au FileType go nmap <LocalLeader>ce :GoCallees<CR>
	au FileType go nmap <LocalLeader>? :GoCoverageToggle<CR>
	au FileType go nmap <LocalLeader>D :GoDefPop<CR>
	au FileType go nmap <LocalLeader>v :GoImplements<CR>
	au FileType go nmap <LocalLeader>I :ThxInstall<CR>
	au FileType go nmap <LocalLeader>y :ThxPlay<CR>
	au FileType go nmap <LocalLeader>' :GoDocBrowser<CR>
	au FileType go nmap <LocalLeader>b :GoToggleBreakpoint<CR>
	au FileType go nmap <LocalLeader>db :GoDebug<CR>
	au FileType go nmap <LocalLeader>re :Refactor extract
	au FileType go nmap <LocalLeader>st <Plug>(go-run-tab)
	au FileType go nmap <LocalLeader>sp <Plug>(go-run-split)
	au FileType go nmap <LocalLeader>vs <Plug>(go-run-vertical)
	au FileType go nmap <LocalLeader>. :GoAlternate<CR>
	au FileType go nmap <LocalLeader>T :GoTestFunc
	au FileType go nmap <LocalLeader>t :GoTest
	au FileType go nmap <LocalLeader>r :GoReferrers<CR>
	au FileType go nmap <LocalLeader>p :GoChannelPeers<CR>
	au FileType go nmap <LocalLeader>d :GoDef<CR>
	au FileType go nmap <LocalLeader>k :GoInfo<CR>
  au FileType go nmap <LocalLeader>i :LspCodeActionSync source.organizeImports<CR>
  au FileType go nnoremap <LocalLeader>e :GoIfErr<CR>
augroup END

"BUFFER
"------------
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
"FILE SEARCH:
"------------
"allows FZF to open by pressing CTRL-F
map <C-p> :FZF<CR>
"allow FZF to search hidden 'dot' files
let $FZF_DEFAULT_COMMAND = "find -L"
nnoremap <silent> <C-f> :call fzf#vim#gitfiles('--exclude-standard --cached --others', {'dir': getcwd()})<CR>
let g:fzf_colors ={ 
  \ 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] 
\ }

"FILE BROWSER:
"-------------
"allows NERDTree to open/close by typing 'n' then 't'
map nt :NERDTreeTabsToggle<CR>
"Start NERDtree when dir is selected (e.g. "vim .") and start NERDTreeTabs
let g:nerdtree_tabs_open_on_console_startup=2
"Add a close button in the upper right for tabs
let g:tablineclosebutton=1
"Automatically find and select currently opened file in NERDTree
let g:nerdtree_tabs_autofind=1
"Add folder icon to directories
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1
"Hide expand/collapse arrows
let g:NERDTreeDirArrowExpandable = "\u00a0"
let g:NERDTreeDirArrowCollapsible = "\u00a0"
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""
highlight! link NERDTreeFlags NERDTreeDir

"SHORTCUTS:
"----------
"Open file at same line last closed
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
  \| exe "normal! g'\"" | endif
endif

"SOURCING:
"---------
"Automatically reloads neovim configuration file on write (w)
autocmd! bufwritepost init.vim source %

"MOUSE:
"------
"Allow using mouse helpful for switching/resizing windows
set mouse+=a
if &term =~ '^screen'
  " tmux knows the extended mouse mode
  set ttymouse=xterm2
endif

"TEXT SEARCH:
"------------
"Makes Search Case Insensitive
set ignorecase

"SWAP:
"-----
set dir=~/.local/share/nvim/swap/

"GIT 
"(FUGITIVE):
"---------------
map fgb :Gblame<CR>
map fgs :Gstatus<CR>
map fgl :Glog<CR>
map fgd :Gdiff<CR>
map fgc :Gcommit<CR>
map fga :Git add %:p<CR>

"Guttet
"-----------
let g:gitgutter_map_keys                     = 0
let g:gitgutter_max_signs                    = 1000
let g:gitgutter_sign_added                   = '▎'
let g:gitgutter_sign_modified                = '▎'
if empty(eval('$VTE_VERSION'))
    let g:gitgutter_sign_removed             = '◢'
    let g:gitgutter_sign_removed_first_line  = '◥'
else " GNOME Terminal bug, see https://is.gd/5vTfFY
    let g:gitgutter_sign_removed             = '▎'
    let g:gitgutter_sign_removed_first_line  = '▎'
endif
let g:gitgutter_sign_modified_removed        = '▌'
let g:gitgutter_sign_removed_above_and_below = '▎'
let g:gitgutter_preview_win_floating         = 1

"SYNTAX HIGHLIGHTING:
"--------------------
syntax on

"HIGHLIGHTING:
"-------------
" <Ctrl-l> redraws the screen and removes any search highlighting.
nnoremap <silent> <C-l> :nohl<CR><C-l>
" Highlight the current line the cursor is on
set cursorline
